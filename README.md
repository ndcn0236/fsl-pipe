[![Documentation](https://img.shields.io/badge/Documentation-fsl--pipe-blue)](https://open.win.ox.ac.uk/pages/fsl/fsl-pipe)
[![File-tree Documentation](https://img.shields.io/badge/Documentation-file--tree-blue)](https://open.win.ox.ac.uk/pages/fsl/file-tree)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6577069.svg)](https://doi.org/10.5281/zenodo.6577069)
[![Pipeline status](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/badges/main/pipeline.svg)](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/-/pipelines/latest)
[![Coverage report](https://git.fmrib.ox.ac.uk/fsl/fsl-pipe/badges/main/coverage.svg)](https://open.win.ox.ac.uk/pages/fsl/fsl-pipe/htmlcov)

FSL-pipe has been moved to https://git.fmrib.ox.ac.uk/fsl/fsl-pipe